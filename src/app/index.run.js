(function() {
  'use strict';

  angular
    .module('cv')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
