(function() {
    'use strict';

    angular
        .module('cv')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController($timeout, $scope) {
        var vm = this;
        vm.form = true;

        vm.professional = [{
            name: 'Adobe Photoshop',
            value: '85'
        }, {
            name: 'HTML',
            value: '90'
        }, {
            name: 'CSS',
            value: '80'
        }, {
            name: 'Javascript',
            value: '100'
        }, {
            name: 'NodeJS',
            value: '95'
        }];

        vm.personal = [{
            name: 'Communication',
            value: '95'
        }, {
            name: 'Team work',
            value: '80'
        }, {
            name: 'Creativity',
            value: '20'
        }, {
            name: 'Dedication',
            value: '90'
        }, {
            name: 'volunteerism',
            value: '83'
        }];

        vm.user = {};
        vm.user.fullname = '';
        vm.user.email = '';
        vm.user.phonenumber = '';
        vm.user.subject = '';
        vm.user.message = '';

        $scope.$watchGroup(['vm.user.fullname', 'vm.user.email', 'vm.user.phonenumber', 'vm.user.subject', 'vm.user.message '], function(n, o, scope) {
            if (n[0] !== '' && n[1] !== '' && n[2] !== '' && n[3] !== '' && n[4] !== '') {
                vm.form = false;
            }
        });
    }
})();